var mail;
var rol;

$(document).ready(function(){

    mostrarNavbarCorrecte();
    
    getUser();
    
    // Quan es faci un submit als formularis no fer res
    $('#mail-form').submit(function () {
        return false;
    });
    
    $('#pass-form').submit(function () {
        return false;
    });
    
    // Mail onclick
    $("#canviarMail").click(function() {
        $("#mail").val(mail);
        $('#modalMail').modal('show');
    });
    
    $("#panelReserves").click(function() {
        window.location.href = 'reserves.html';
    });
                              
    $("#panelDadesCompte").click(function() {
        window.location.href = 'panelControl.html';
    });
    
    $("#crearGimnas").click(function() {
        window.location.href = 'crearGimnas.html';
    });
    
    $("#editarGimnas").click(function() {
        window.location.href = 'editarGimnas.html';
    });


});

function borrarUsuari() {
    var parametres = {
        usuariID: getCookie("usuariActual")
    };
        
    var post = $.post("../php/controller/borrarUsuari.php", parametres, respostaBorrarUsuari, 'json');
}

function respostaBorrarUsuari(r) {
    if (r.error == '0') {
         window.location.href = 'index.html';
    }
}

function canviarMail() {
     $("#passError").html("");
        
    pass1 = $("#pass1").val();
    pass2 = $("#pass2").val();
    pass3 = $("#pass3").val();
    // Els camps no poden estar buits
    if (pass1 == "" || pass2 == "" || pass3 == "") {
         $("#passError").html("Has de completar tots els camps");
        return;
    }
    
    // Validar que la nova contrasenya no sigui massa feble
    // La contrasenya no pot tenir menys de 6 caràcters
    if (pass2.length < 6) {
        $("#passError").html("La nova contrasenya es massa feble: no pot tenir menys de 6 caràcters");
        return;
    }
    // La contrasenya ha de tenir al menys un número
    expresioRegular = /[0-9]+/;
    if(!expresioRegular.test(pass2)) {
        $("#passError").html("La nova contrasenya es massa feble: ha de tenir al menys un número");
        return;
    }
    // La contrasenya ha de tenir al menys una lletra
    expresioRegular = /[a-z]+/;
    if (!expresioRegular.test(pass2)) {
       $("#passError").html("La nova contrasenya es massa feble: ha de tenir al menys una lletra");
        return;
    }
    // Comprobar que les dos contrasenyes son la mateixa
    if (pass3 != pass2) {
        $("#passError").html("Les dos contrasenyes no coincideixen");
        return;
    }
    
    // Es canvia la contrasenya
    var parametres = {
        usuariID: getCookie("usuariActual"),
        passAntiga: pass1,
        passNova: pass2
    };
        
    var post = $.post("../php/controller/canviarPass.php", parametres, respostaCanviarPass, 'json');
}

function respostaCanviarPass(r) {
    if (r.error == '0') {
        $('#modalPass').modal('hide');
        
        $("#titolModal").html("Contrasenya canviada");
        $("#missatgeError").html("");
        $("#missatgeInformatiu").html("La contrasenya s'ha actualitzat correctament.");
        $('#modalMissatge').modal('show');
    } else if (r.error == '1') {
        $("#passError").html(r.missatgeError);
    }
}

function mailSubmit(){
    
    nouMail = $("#mail").val();
    
    // Es comproba que el mail sigui vàlid
    expresioRegular = /^([\S.]+\@[\S.]+\.[\S.]+)$/;
    if (!expresioRegular.test(nouMail)) {
         $("#mailError").html("El mail no ès valid");
        return;
    }
    
    // Es canvia el mail
    var parametres = {
        usuariID: getCookie("usuariActual"),
        mail: nouMail
    };
    
    var post = $.post("../php/controller/canviarMail.php", parametres, respostaCanviarMail, 'json');
}

function respostaCanviarMail(r) {
    if (r.error == "0") {
        // Mail actualitzat (es recarga la pàgina)
        window.location.href = 'panelControl.html';
    } else if (r.error == '1') {
        $('#modalMail').modal('hide');
        
        $("#titolModal").html("Error");
        $("#missatgeError").html(r.missatgeError);
        $("#missatgeInformatiu").html("");
        $('#modalMissatge').modal('show');
    }  
}

function getUser() {
    // Funció que demana les dadel del usuari actual al servidor
    var parametres = {
        usuariID: getCookie("usuariActual")
    };
    
    var post = $.post("../php/controller/obtenirUsuari.php", parametres, respostaObtenirUsuari, 'json');
}

function respostaObtenirUsuari(r) {
    mail = r.mail;
    rol = r.rol;
    
    $("#nomUser").append(r.nom);
    $("#mailUser").append(mail);
    
    // Mostrar les opcions d'administrador si el usuari te rol d'administrador.
    // Tampoc deixa esborrar el usuari administrador
    if (rol == 'administrador') {
        $("#crearGimnas").css({"visibility":"visible"});
        $("#borrarUsuari").css({"visibility":"hidden"});
        $("#editarGimnas").css({"visibility":"visible"});
        
    }
}
                  
function mostrarNavbarCorrecte() {

     // Depenen de si estem loguegats o no mostrem unes opcions o altres al navbar
    var nomUser = getCookie("usuariActual");
    if ( nomUser == "") {
        // No hi ha cap usuari loguegat
        $("#navbarDropdownUser").remove();
  
    } else {
        // Usuari loguegat
        //$("#navbarOpcions").children().length;
        $("#navbarSignUp").remove();
        $("#navbarLogin").remove();
        $("#navbarDropdownUserNom").html(nomUser+" <span class='caret'></span>");
        
    }
}

// Funció que mostra si una cookie està creada o no (agafada de W3School)
function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1);
		if (c.indexOf(name) != -1) {
			return c.substring(name.length,c.length);
		}
	}
	return "";
}
