var mail;
var rol;

$(document).ready(function(){
    
    //alert($("#nomGimnas").val());

    mostrarNavbarCorrecte();
    
    getUser();
    
    // Quan es faci un submit als formularis no fer res
    $('#mail-form').submit(function () {
        return false;
    });
    
    $('#pass-form').submit(function () {
        return false;
    });
    
    $("#panelReserves").click(function() {
        window.location.href = 'reserves.html';
    });
                              
    $("#panelDadesCompte").click(function() {
        window.location.href = 'panelControl.html';
    });
    
    $("#crearGimnas").click(function() {
        window.location.href = 'crearGimnas.html';
    });
    
    $("#editarGimnas").click(function() {
        window.location.href = 'editarGimnas.html';
    });
    
    $("#seleccionarClasse").click(function() {
        $('#modalClasse').modal('show');
    });
    
    // Nomes seleccionar un checkbox alhora
    $('input[type="checkbox"]').on('change', function() {
        $('input[name="' + this.name + '"]').not(this).prop('checked', false);
    });

});


function getUser() {
    // Funció que demana les dadel del usuari actual al servidor
    var parametres = {
        usuariID: getCookie("usuariActual")
    };
    
    var post = $.post("../php/controller/obtenirUsuari.php", parametres, respostaObtenirUsuari, 'json');
}

function respostaObtenirUsuari(r) {
    mail = r.mail;
    rol = r.rol;
    
    $("#nomUser").append(r.nom);
    $("#mailUser").append(mail);
    
    // Mostrar les opcions d'administrador si el usuari te rol d'administrador
    if (rol == 'administrador') {
        $("#crearGimnas").css({"visibility":"visible"});
        $("#editarGimnas").css({"visibility":"visible"});
    }
}
                  
function mostrarNavbarCorrecte() {

     // Depenen de si estem loguegats o no mostrem unes opcions o altres al navbar
    var nomUser = getCookie("usuariActual");
    if ( nomUser == "") {
        // No hi ha cap usuari loguegat
        $("#navbarDropdownUser").remove();
  
    } else {
        // Usuari loguegat
        //$("#navbarOpcions").children().length;
        $("#navbarSignUp").remove();
        $("#navbarLogin").remove();
        $("#navbarDropdownUserNom").html(nomUser+" <span class='caret'></span>");
        
    }
}

function crearGimnas() {
    
    $("#modalClasse").modal('hide');
    
    // Control d'errors
    nom = $("#GimnasNom").val();
    latitud = $("#GimnasLatitud").val();
    longitud = $("#GimnasLongitud").val();
    direccio = $("#GimnasDireccio").val();
    horari = $("#GimnasHorari").val();
    minutsEntreSetmana = $("#GimnasMinutsEntreSetmana").val();
    horesEntreSetmana = $("#GimnasHoresEntreSetmana").val();
    minutsCapSetmana = $("#GimnasMinutsCapSetmana").val();
    horesCapSetmana = $("#GimnasHoresCapSetmana").val();
    
    paginaReserva = "";
    llocsDisponibles = "";
    if ($("#boxClasse1").is(':checked')) {
        paginaReserva = 'reserva1.html';
        llocsDisponibles = '14';
    }
    if ($("#boxClasse2").is(':checked')) {
        paginaReserva = 'reserva2.html';
        llocsDisponibles = '9';
    }
    if ($("#boxClasse3").is(':checked')) {
        paginaReserva = 'reserva3.html';
        llocsDisponibles = '14';
    }
    if ($("#boxClasse4").is(':checked')) {
        paginaReserva = 'reserva4.html';
        llocsDisponibles = '10';
    }
    
    // S'ha de marcar algun checkbox
    if (paginaReserva == "") {
        mostraError("Has de seleccionar alguna classe");
        return;
    }
    
     // Els camps no poden estar buits
    if (nom == "" || latitud == "" || longitud == "" || direccio == "" || horari == "" || minutsEntreSetmana == "" || minutsCapSetmana == "") {
         mostraError("Has de completar tots els camps");
        return;
    }
    
    // El nom del gimnàs ha de tenir almenys 4 caràcters
    if (nom.length < 4) {
         mostraError("El nom del gimnàs ha de tenir almenys 4 caràcters");
        return;
    }
    
    // Si hi ha classes entre setmana mirar que hi hagin hores posades
    
    if (minutsEntreSetmana != '0') {
        if (horesEntreSetmana == "") {
            mostraError("Has de indicar les hores per les classes entre setmana");
        return;
        }
    }
    
    // Si hi ha classes en el cap setmana mirar que hi hagin hores posades
    if (minutsCapSetmana != "0") {
        if (horesCapSetmana == "") {
            mostraError("Has de indicar les hores per les classes en cap de setmana");
        return;
        }
    }
    
    // Si no hi ha hagut errors registrar el nou gimnàs
    var parametres = {
        nom: nom,
        latitud : latitud ,
        longitud: longitud,
        direccio: direccio,
        horari: horari,
        minutsEntreSetmana: minutsEntreSetmana,
        horesEntreSetmana: horesEntreSetmana,
        minutsCapSetmana: minutsCapSetmana,
        horesCapSetmana: horesCapSetmana,
        paginaReserva: paginaReserva,
        llocsDisponibles: llocsDisponibles
    };

    var post = $.post("../php/controller/crearGimnas.php", parametres, respostaCrearGimnas, 'json');
    
}

function respostaCrearGimnas(r) {
    if (r.error == '0') {
        // Alta correcte
        $("#titolModal").html("Alta correcte");
        $("#missatgeError").html("");
        $("#missatgeInformatiu").html("Gimnàs donat d'alta correctament");
        $('#modalMissatge').modal('show');
    } else if (r.error == '1') {
        mostraError(r.missatgeError);
    }
}

function mostraError(textError) {
    $("#titolModal").html("Error");
    $("#missatgeError").html(textError);
    $("#missatgeInformatiu").html("");
    $('#modalMissatge').modal('show');
}

// Funció que mostra si una cookie està creada o no (agafada de W3School)
function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1);
		if (c.indexOf(name) != -1) {
			return c.substring(name.length,c.length);
		}
	}
	return "";
}
