var IMAGE = "img/icon-gym.png";

var map;
var array;

function initialize() {
    
    // Fer que el div del mapa ocupi només fins el final de la pantalla
    //var viewportHeight = $(window).height();
    //var offsetTop = $(".navbar").height();
    //$("#map-canvas").css("height", viewportHeight - offsetTop ); 
    //$(window).resize();
    
    mostrarNavbarCorrecte();
    
    // Quan es faci un submit als formularis no fer res
    $('#login-form').submit(function () {
        return false;
    });
    $('#registre-form').submit(function () {
        return false;
    });
    
    dibuixaMapa();

    dibuixaGimnasos();
    
}

function onClickLogin() {
    
    // CONTROL D'ERRORS
    nom = $("#usuariLogin").val();
    pass = $("#passLogin").val();
    
     // Els camps no poden estar buits
    if (nom == "" || pass == "") {
         $("#loginError").html("Has de completar tots els camps");
        return;
    }
    
    var parametres = {
        usuari: nom,
        pass: pass
    };
    
    var post = $.post("../php/controller/login.php", parametres, respostaLogin, 'json');
    
}


function onClickRegistre() {
    
     $("#registreError").html("");
    
    // CONTROL D'ERRORS
    nom = $("#usuariRegistre").val();
    pass = $("#passRegistre").val();
    pass2 = $("#pass2Registre").val();
    mail = $("#mailRegistre").val();
    
     // Els camps no poden estar buits
    if (nom == "" || pass == "" || pass2 == "" || mail == "") {
         $("#registreError").html("Has de completar tots els camps");
        return;
    }
    
    // El nom del usuari ha de tenir almenys 4 caràcters
    if (nom.length < 4) {
        $("#registreError").html("El nom del usuari no pot tenir menys de 4 caràcters");
        return;
    }
    
    // Validar que la contrasenya no sigui massa feble
    // La contrasenya no pot tenir menys de 6 caràcters
    if (pass.length < 6) {
        $("#registreError").html("La contrasenya es massa feble: no pot tenir menys de 6 caràcters");
        return;
    }
    // La contrasenya ha de tenir al menys un número
    expresioRegular = /[0-9]+/;
    if(!expresioRegular.test(pass)) {
        $("#registreError").html("La contrasenya es massa feble: ha de tenir al menys un número");
        return;
    }
    // La contrasenya ha de tenir al menys una lletra
    expresioRegular = /[a-z]+/;
    if (!expresioRegular.test(pass)) {
       $("#registreError").html("La contrasenya es massa feble: ha de tenir al menys una lletra");
        return;
    }
    // Comprobar que les dos contrasenyes son la mateixa
    if (pass != pass2) {
        $("#registreError").html("Les dos contrasenyes no coincideixen");
        return;
    }
    // Validar el mail
    expresioRegular = /^([\S.]+\@[\S.]+\.[\S.]+)$/;
    if (!expresioRegular.test(mail)) {
         $("#registreError").html("El mail es incorrecte");
        return;
    }
    
    // Si no hi ha hagut errors fer el registre
    var parametres = {
        usuari: nom,
        pass: pass,
        pass2: pass2,
        mail: mail
    };

    var post = $.post("../php/controller/crearUsuari.php", parametres, respostaCrearUsuari, 'json');
}
    

function respostaLogin(r) {
    if (r.error == '0') {
        // Login correcte (recargar la pàgina)
        window.location.href = 'map.html';
    } else if (r.error == '1') {
        $("#loginError").html(r.missatgeError);
    }        
}

function respostaCrearUsuari(r) {    
    if (r.error == '0') {
        // Alta correcte (recargar la pàgina)
        window.location.href = 'index.html';
    } else if (r.error == '1') {
        $("#registreError").html(r.missatgeError);
    }          
}

function mostrarNavbarCorrecte() {
     // Depenen de si estem loguegats o no mostrem unes opcions o altres al navbar
    var nomUser = getCookie("usuariActual");
    if ( nomUser == "") {
        // No hi ha cap usuari loguegat
        $("#navbarDropdownUser").remove();
  
    } else {
        // Usuari loguegat
        //$("#navbarOpcions").children().length;
        $("#navbarSignUp").remove();
        $("#navbarLogin").remove();
        $("#navbarDropdownUserNom").html(nomUser+" <span class='caret'></span>");
        
    }
}

function onClickReserva(idGimnas, paginaReserva) {
    
    // Si intentem fer una reserva sense estar loguegats mostrem el panel de login
    var nomUser = getCookie("usuariActual");
    if ( nomUser == "") {
        $('#modalLogin').modal('show');
    } else {

        // Es guarda en una cookie temporal quin es el gimnas
        document.cookie="gimnasReserva="+idGimnas+"; expires=0; path=/";
        
        // Es va a la pàgina de la reserva
        window.location.href = paginaReserva;
    }
    
}

function dibuixaGimnasos() {
    
    $.getJSON("../php/controller/obtenirGimnasos.php", function (result) {

         $.each(result, function (i, field) { 
             // Dibuixar al mapa cada gimnàs amb els seus valors
             // Els gimnasos son rebuts en format JSON
             var marker = new google.maps.Marker({
                 position:  new google.maps.LatLng(field.latitud,field.longitud),
                 map: map,
                 title:field.nom,
                 icon: IMAGE
             });
             var contentString = '<div id="panelGimnas"><h2 id="titolGimnas">'+ field.nom +'</h2>' 
             + '<p>Direcció: '+field.direccio+'</p>' + '<p>Horari: '+field.horari+'</p>'
             + '<p>Màquines disponibles: '+field.llocsdisponibles+'</p>'
             if (field.llocsdisponibles > 0) {
                 contentString = contentString + '<button type="button" class="col-sm-12 col-sm-offset-0 btn btn-primary" onClick="onClickReserva('+field.id+', \''+field.paginareserva+'\')" >Reserva</button>';
             } else {
                 contentString = contentString + '<button type="button" class="col-sm-12 col-sm-offset-0 btn btn-danger">No hi ha places</button></div>';
             }  
             
             var infowindow = new google.maps.InfoWindow({
                 content: contentString
             });
        
             google.maps.event.addListener(marker, 'click', function() {
                 infowindow.open(map,marker);
             });
         });
    }); 
}



function dibuixaMapa() {
    if( navigator.geolocation != null) {
					
        var mapOptions = {
            //center: { lat: 41.388843, lng: 12.148334}, 
            //mapTypeId: google.maps.MapTypeId.HYBRID, 
            zoom: 17
        };
        
        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            map.setCenter(pos);
        }, function () {
            // No hi ha permisos per fer la geolocalitzacio. S'una una posició per defecte
            var mapOptions = {
                center: { lat: 41.388843, lng: 2.148334}, 
                //mapTypeId: google.maps.MapTypeId.HYBRID, 
                zoom: 17
            };
            map=new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
            dibuixaGimnasos();
            
            // Mostra un missatge indicant que no es té permisos de geolocalització
            $("#titolModal").html("Missatge informatiu");
            $("#missatgeInformatiu").html("No s'ha pogut obtenir la geolocalització. No hi ha permisos. Es mostra unes coordenades per defecte.");
            $("#missatgeError").html("");
            $('#modalMissatge').modal('show');
            //$("#map-canvas").html("No s'ha pogut obtenir la geolocalització. No hi ha permisos");
        });
					
    } else {
        // Navegador no pot obtenir la geolocalitzacio
        $("#map-canvas").html("No s'ha pogut obtenir la geolocalització");
    }
				
}

// Funció que mostra si una cookie està creada o no (agafada de W3School)
function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1);
		if (c.indexOf(name) != -1) {
			return c.substring(name.length,c.length);
		}
	}
	return "";
} 
		
google.maps.event.addDomListener(window, 'load', initialize);
			
