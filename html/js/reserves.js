var gimnasos = [];
var diaActual;
var horaActual;

$(document).ready(function(){

    mostrarNavbarCorrecte();
    
    getUser();
    
    // Per mostrar el nom de cada gimnas, fem una petició per obtenir la llista de tots els gimnasos i així tenir tots els noms en una sola petició
    $.getJSON("../php/controller/obtenirGimnasos.php", function (result) {

         $.each(result, function (i, field) {
             gimnasos[field.id] = field.nom; 
         });
        
        // Després de rebre la llista de gimnasos es demana la llista de reserves del usuari
         getReserves();
    }); 
    
    $("#panelReserves").click(function() {
        window.location.href = 'reserves.html';
    });
                              
    $("#panelDadesCompte").click(function() {
        window.location.href = 'panelControl.html';
    });
    
    $("#crearGimnas").click(function() {
        window.location.href = 'crearGimnas.html';
    });
    
    $("#editarGimnas").click(function() {
        window.location.href = 'editarGimnas.html';
    });
    
});

function clickEliminar(id) {
    var parametres = {
        usuariID: getCookie("usuariActual"),
        gimnasID: id.split("/")[0],
        dia: id.split("/")[1],
        hora: id.split("/")[2]
    };
    
    var post = $.post("../php/controller/eliminarReserva.php", parametres, respostaEliminarReserva, 'json');
}

function respostaEliminarReserva(r) {
    if (r.error == '0') {
        window.location.href = 'reserves.html';
    } else if (r.error == '1') {
        $("#titolModal").html("Error");
        $("#missatgeError").html("Error al eliminar la reserva");
        $("#missatgeInformatiu").html("");
        $('#modalMissatge').modal('show');
        return;
    }
}
                  
function mostrarNavbarCorrecte() {

     // Depenen de si estem loguegats o no mostrem unes opcions o altres al navbar
    var nomUser = getCookie("usuariActual");
    if ( nomUser == "") {
        // No hi ha cap usuari loguegat
        $("#navbarDropdownUser").remove();
  
    } else {
        // Usuari loguegat
        //$("#navbarOpcions").children().length;
        $("#navbarSignUp").remove();
        $("#navbarLogin").remove();
        $("#navbarDropdownUserNom").html(nomUser+" <span class='caret'></span>");
        
    }
}

function getReserves() {

    var d = new Date()
    year = d.getFullYear();
    day = d.getDate();
    month = d.getMonth()+1;
    if (month.toString().length == 1) {
        month = "0" + month;
    }
    dia = year + "-" + month + "-" + day;
    hour = d.getHours();
    
    diaActual = dia;
    horaActual = hour;

    var parametres = {
        usuariID: getCookie("usuariActual"),
		dia: dia,
		hora: hour
    };
    
    var post = $.post("../php/controller/obtenirReservesUsuari.php", parametres, respostaObtenirReservesUsuari, 'json');
}

function respostaObtenirReservesUsuari(r) {
    if (r.length == 0) {
         $(".dades").append("<p>No tens cap reserva feta.</p>");
    }
    $.each(r, function (i, field) {
        // No mostrar les reserves del dia actual que ja hagin passat
        if (field.dia == diaActual && field.hora <= horaActual) {
        } else {
        
            idButton = field.gimnasid + "/" + field.dia + "/" + field.hora;
            //alert(field.dia + " - " + field.hora);
            text = "<button class='minimal-indent'>&nbsp;&nbsp;&nbsp;Dia "+field.dia+" a les "+field.hora+' hores en el gimnàs "'+gimnasos[field.gimnasid]+'"&nbsp;&nbsp;&nbsp;</button><button type="button" id="'+idButton+'" class="btn btn-danger" onClick="clickEliminar(this.id)">Eliminar</button></br>'
            $(".dades").append(text);
        }
    });
    
}

function getUser() {
    // Funció que demana les dadel del usuari actual al servidor
    var parametres = {
        usuariID: getCookie("usuariActual")
    };
    
    var post = $.post("../php/controller/obtenirUsuari.php", parametres, respostaObtenirUsuari, 'json');
}

function respostaObtenirUsuari(r) {
    rol = r.rol;
    
    // Mostrar les opcions d'administrador si el usuari te rol d'administrador
    if (rol == 'administrador') {
        $("#crearGimnas").css({"visibility":"visible"});
        $("#editarGimnas").css({"visibility":"visible"});
    }
}

// Funció que mostra si una cookie està creada o no (agafada de W3School)
function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1);
		if (c.indexOf(name) != -1) {
			return c.substring(name.length,c.length);
		}
	}
	return "";
}
