var duracioEntreSetmana;
var horesEntreSetmana;
var duracioCapSetmana;
var horesCapSetmana;
var llocsDisponibles;
var numBici = "";

$(document).ready(function(){
    
    // Obtenir les dades del gimnàs
    var parametres = {
        gimnasID: getCookie("gimnasReserva")
    };
    
    var post = $.post("../php/controller/obtenirDadesGimnas.php", parametres, respostaObtenirDadesGimnas, 'json');

    
    mostrarNavbarCorrecte();
    
    $("#hores").change(function() {
         bicisDefault();
        obtenirReservesClasse(); 
    });
    
    
    // Afegim el listener de les bicis
    $(".bici").click(function() {
        // Recorrer les bicis no ocupades i deixarles en negre
        if (numBici != "") {
            id = "#bici"+numBici;
            $(id).attr('src','img/bike.png');
        }
        numBici = $(this).attr('id').split("bici")[1];
        $(this).attr('src','img/bikeSeleccionada.png');
    });

});

function bicisDefault() {
    $(".bici").each(function(i) {
        $(this).attr('src','img/bike.png');
    });
    $(".bici").click(function() {
       // Recorrer les bicis no ocupades i deixarles en negre
        if (numBici != "") {
            id = "#bici"+numBici;
            $(id).attr('src','img/bike.png');
        }
        numBici = $(this).attr('id').split("bici")[1];
        $(this).attr('src','img/bikeSeleccionada.png');
    });
    numBici = "";
}

function respostaObtenirDadesGimnas(field) {
        $("#nomGimnas").html(field.nom);
        llocsDisponibles = field.llocsdisponibles;
        duracioEntreSetmana = field.duracioreservesentresetmana;
        duracioCapSetmana = field.duracioreservescapsetmana;
        // Posar el calendari acord als dies que hi ha o no hi han reserves
        var dia = new Date().getDate();
        var mes = new Date().getMonth() + 1;
        var any = new Date().getFullYear();
    
        diaInici = any+"-"+mes+"-"+dia;
        diaFinal = any+"-"+(mes+2)+"-"+dia;
        // Datepicker
        if (duracioEntreSetmana != "0" && duracioCapSetmana != "0") {
            // Tots els dies permesos
             $('#sandbox-container').datepicker({
                format: "yyyy-mm-dd",
                weekStart: 1,
                startDate: diaInici,
                endDate: diaFinal,
                language: "ca",
                todayHighlight: true
            }).on("changeDate", function(e){
                 generarHores(e.date);
                 bicisDefault();
                 obtenirReservesClasse()
            });
        } else if (duracioEntreSetmana != "0" && duracioCapSetmana == "0") {
            // Nomes dies entre setmana
            $('#sandbox-container').datepicker({
                format: "yyyy-mm-dd",
                weekStart: 1,
                startDate: diaInici,
                endDate: diaFinal,
                language: "ca",
                todayHighlight: true,
                daysOfWeekDisabled: "0,6"
            }).on("changeDate", function(e){
                 generarHores(e.date);
                bicisDefault();
                obtenirReservesClasse()
            });
        } else if (duracioEntreSetmana == "0" && duracioCapSetmana != "0") {
            // Nomes caps de setmana
            $('#sandbox-container').datepicker({
                format: "yyyy-mm-dd",
                weekStart: 1,
                startDate: diaInici,
                endDate: diaFinal,
                language: "ca",
                todayHighlight: true,
                daysOfWeekDisabled: "1,2,3,4,5"
            }).on("changeDate", function(e){
                 generarHores(e.date);
                bicisDefault();
                obtenirReservesClasse()
            });
        } else if (duracioEntreSetmana == "0" && duracioCapSetmana == "0") {
            // Cap dia
            $('#sandbox-container').datepicker({
                format: "yyyy-mm-dd",
                weekStart: 1,
                startDate: diaInici,
                endDate: diaFinal,
                language: "ca",
                todayHighlight: true,
                daysOfWeekDisabled: "0,1,2,3,4,5,6"
            }).on("changeDate", function(e){
                 generarHores(e.date);
                bicisDefault();
                obtenirReservesClasse()
            });
        }
        horesEntreSetmana = field.horespermesesreservarentresetmana;
        horesCapSetmana = field.horespermesesreservarcapsetmana;;

}

function generarHores(dia) {
    // Funció que depenen del dia escollit, genera les hores de les clases disponibles aquest dia
    // Primer esborrem les opciones que hi habien abans
    document.getElementById('hores').options.length = 0;
    
    // Diferenciem si es cap de setmana o no
    var day = dia.getDay();
    var isWeekend = (day == 6) || (day == 0);    // 6 = Saturday, 0 = Sunday
    horesInici = "";
    minutsDuracio = "";
    if (isWeekend) {
        horesInici = horesCapSetmana;
        minutsDuracio = duracioCapSetmana;
    } else {
        horesInici = horesEntreSetmana;
        minutsDuracio = duracioEntreSetmana;
    }
    
    // Calculem la data final de cada resera i mostrem les hores de reserva
    selectHora = document.getElementById('hores');
    var hores = horesInici.split(","); 
    for (i=0;i<hores.length;i++) {
        horaFinal = Math.floor(parseInt(hores[i]) + parseInt(minutsDuracio)/60);
        minutFinal = parseInt(minutsDuracio)%60;
        if (minutFinal.toString().length == 1) {
             minutFinal = "0" + minutFinal;
         }
        opcio = hores[i] + ":00 - " + horaFinal + ":" + minutFinal;
        selectHora.options[selectHora.options.length] = new Option(opcio, hores[i]); 
     }
    
}

function onClickReserva() {
    
    dataReserva = $("#data").val();
    select = document.getElementById("hores");
    horaReserva = "";
    if (select.options.length != 0) {
        horaReserva = select.options[select.selectedIndex].value;
    }
    
    // Es comproba que la data i hora no estiguin buits
    if (dataReserva == "" || horaReserva == "") {
        
        $("#titolModal").html("Error");
        $("#missatgeError").html("Has de especificar una data i una hora");
        $("#missatgeInformatiu").html("");
        $('#modalMissatge').modal('show');
        return;
        
    }
    // Es comproba que no es resrevi una hora ja pasada
    var d = new Date()
    year = d.getFullYear();
    day = d.getDate();
    month = d.getMonth()+1;
    if (month.toString().length == 1) {
        month = "0" + month;
    }
    dia = year + "-" + month + "-" + day;
    if (dataReserva == dia) {
        hour = d.getHours();
        if (horaReserva <= hour) {
            $("#titolModal").html("Error");
            $("#missatgeError").html("Aquesta classe ja ha acabat o acaba de començar. Tria una altre.");
            $("#missatgeInformatiu").html("");
            $('#modalMissatge').modal('show');
            return;
        }
    }
    // Es comproba que s'hagi escogit una bici
    if (numBici == "") {
        $("#titolModal").html("Error");
        $("#missatgeError").html("Has de marcar una bici");
        $("#missatgeInformatiu").html("");
        $('#modalMissatge').modal('show');
        return;
    }
        
     var parametres = {
         gimnasID: getCookie("gimnasReserva"),
         usuariID: getCookie("usuariActual"),
         data: dataReserva,
         hora: horaReserva,
         bici: numBici
     };
     
     var post = $.post("../php/controller/reservar.php", parametres, respostaReserva, 'json');
    
}

function respostaReserva(r) {
    if (r.error == '0') {
        // Reserva correcte
        $("#titolModal").html("Reserva feta");
        $("#missatgeInformatiu").html("Bicicleta reservada correctament");
        $("#missatgeError").html("");
        $('#modalMissatge').modal('show');
    } else if (r.error == '1') {
        // Error
        $("#titolModal").html("Error");
        $("#missatgeError").html(r.missatgeError);
        $("#missatgeInformatiu").html("");
        $('#modalMissatge').modal('show');
    }
}

function mostrarNavbarCorrecte() {
     // Depenen de si estem loguegats o no mostrem unes opcions o altres al navbar
    var nomUser = getCookie("usuariActual");
    if ( nomUser == "") {
        // No hi ha cap usuari loguegat
        $("#navbarDropdownUser").remove();
  
    } else {
        // Usuari loguegat
        //$("#navbarOpcions").children().length;
        $("#navbarSignUp").remove();
        $("#navbarLogin").remove();
        $("#navbarDropdownUserNom").html(nomUser+" <span class='caret'></span>");
        
    }
}

function obtenirReservesClasse() {
    
    gimnasID = getCookie("gimnasReserva");
    dataReserva = $("#data").val();
    select = document.getElementById("hores");
    horaReserva = "";
    if (select.options.length != 0) {
        horaReserva = select.options[select.selectedIndex].value;
    }
    
    var parametres = {
        gimnasID: gimnasID,
        data: dataReserva,
        hora: horaReserva
    };
    
    var post = $.post("../php/controller/obtenirReservesClasse.php", parametres, respostaObtenirReservesClasse, 'json');
}

function respostaObtenirReservesClasse(r) {
    $.each(r, function (i, field) {
        // Poso cada bici reservada en un altre color i no es podrà seleccionar
        id = "#bici"+field;
        $(id).attr('src','img/bikeOcupada.png');
        $(id).unbind('click');
    });
}

// Funció que mostra si una cookie està creada o no (agafada de W3School)
function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1);
		if (c.indexOf(name) != -1) {
			return c.substring(name.length,c.length);
		}
	}
	return "";
} 
