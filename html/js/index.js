$(document).ready(function(){
	// Canviem la imatge de fons cada 3 segons (NO S'UTILITZA)
	//setInterval('swapImages()', 3000);
	
	// Utilitzem la llibreria bgswitcher per canviar la imatge de fons del body de manera animada cada cert temps
	$("body").bgswitcher({
		images: ["img/home2v2.jpg", "img/home4v2.png", "img/home3v2.jpg"],
		interval: 5000,  // Interval of switching
		effect: "fade",  // Effect type
		duration: 1000, // Effect duration
		easing: "swing", // Effect easing
		loop: true // Loop the switch
		
	});
    
    // Quan es faci un submit als formularis no fer res
    $('#login-form').submit(function () {
        return false;
    });
    $('#registre-form').submit(function () {
        return false;
    });
    
    mostrarNavbarCorrecte();
    
    //document.getElementById("info").addEventListener('click',alert("test"),true);
    
    // Mostrar info al passar el ratoli per sobre la imatge de info
    $("#info").bind('click', function(event) {
        event.stopPropagation();
        $(".descripcio").toggleClass("descripcio2");
    });
    
});

function onClickLogin() {
    
    // CONTROL D'ERRORS
    nom = $("#usuariLogin").val();
    pass = $("#passLogin").val();
    
     // Els camps no poden estar buits
    if (nom == "" || pass == "") {
         $("#loginError").html("Has de completar tots els camps");
        return;
    }
    
    var parametres = {
        usuari: nom,
        pass: pass
    };
    
    var post = $.post("../php/controller/login.php", parametres, respostaLogin, 'json');
    
}

function onClickRegistre() {
    
     $("#registreError").html("");
    
    // CONTROL D'ERRORS
    nom = $("#usuariRegistre").val();
    pass = $("#passRegistre").val();
    pass2 = $("#pass2Registre").val();
    mail = $("#mailRegistre").val();
    
     // Els camps no poden estar buits
    if (nom == "" || pass == "" || pass2 == "" || mail == "") {
         $("#registreError").html("Has de completar tots els camps");
        return;
    }
    
    // El nom del usuari ha de tenir almenys 4 caràcters
    if (nom.length < 4) {
        $("#registreError").html("El nom del usuari no pot tenir menys de 4 caràcters");
        return;
    }
    
    // Validar que la contrasenya no sigui massa feble
    // La contrasenya no pot tenir menys de 6 caràcters
    if (pass.length < 6) {
        $("#registreError").html("La contrasenya es massa feble: no pot tenir menys de 6 caràcters");
        return;
    }
    // La contrasenya ha de tenir al menys un número
    expresioRegular = /[0-9]+/;
    if(!expresioRegular.test(pass)) {
        $("#registreError").html("La contrasenya es massa feble: ha de tenir al menys un número");
        return;
    }
    // La contrasenya ha de tenir al menys una lletra
    expresioRegular = /[a-z]+/;
    if (!expresioRegular.test(pass)) {
       $("#registreError").html("La contrasenya es massa feble: ha de tenir al menys una lletra");
        return;
    }
    // Comprobar que les dos contrasenyes son la mateixa
    if (pass != pass2) {
        $("#registreError").html("Les dos contrasenyes no coincideixen");
        return;
    }
    // Validar el mail
    expresioRegular = /^([\S.]+\@[\S.]+\.[\S.]+)$/;
    if (!expresioRegular.test(mail)) {
         $("#registreError").html("El mail es incorrecte");
        return;
    }
    
    // Si no hi ha hagut errors fer el registre
    var parametres = {
        usuari: nom,
        pass: pass,
        pass2: pass2,
        mail: mail
    };

    var post = $.post("../php/controller/crearUsuari.php", parametres, respostaCrearUsuari, 'json');
}
    

function respostaLogin(r) {
    if (r.error == '0') {
        // Login correcte (recargar la pàgina)
        window.location.href = 'index.html';
    } else if (r.error == '1') {
        $("#loginError").html(r.missatgeError);
    }        
}

function respostaCrearUsuari(r) {    
    if (r.error == '0') {
        // Alta correcte (recargar la pàgina)
        window.location.href = 'index.html';
    } else if (r.error == '1') {
        $("#registreError").html(r.missatgeError);
    }          
}


function mostrarNavbarCorrecte() {
     // Depenen de si estem loguegats o no mostrem unes opcions o altres al navbar
    var nomUser = getCookie("usuariActual");
    if ( nomUser == "") {
        // No hi ha cap usuari loguegat
        $("#navbarDropdownUser").remove();
  
    } else {
        // Usuari loguegat
        //$("#navbarOpcions").children().length;
        $("#navbarSignUp").remove();
        $("#navbarLogin").remove();
        $("#navbarDropdownUserNom").html(nomUser+" <span class='caret'></span>");
        
    }
}

// Funció que mostra si una cookie està creada o no (agafada de W3School)
function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1);
		if (c.indexOf(name) != -1) {
			return c.substring(name.length,c.length);
		}
	}
	return "";
} 

function clickMaps() {
    window.location.href = 'map.html';
}

function swapImages(){
	// Funció que intercanviaba la imatge d'un div cada cert temps. 
	// NO S'UTILITZA. En el seu cas s'utilitza la llibreria bgswitcher.
	
	// Guardem quina imatge està activa
	var actiu = $(".actiu");
	// Guardem la següent imatge. Si es la última agafem la primera de la galería
	var next;
	if ($("#galeria").children().last().attr("class") == "actiu") {
		next = $("#galeria").children().first();
	} else {
		next = $(".actiu").next();
	}
	
	// Eliminem la classe actiu de la imatge activa
	actiu.removeClass("actiu");
	// Afegim la classe actiu a la imatge que toca
	next.addClass("actiu");
	
	// Mostrem nomes la imatge que té la classe "actiu" i les demes les ocultem
	$("#galeria").children().each(function(i) {

		if ($(this).attr("class") == "actiu") {
			$(this).fadeIn();
		} else {
			$(this).fadeOut();
		}
	});
}
