$(document).ready(function(){

    mostrarNavbarCorrecte();
    
    getUser();
    
    // Per mostrar el nom de cada gimnas, fem una petició per obtenir la llista de tots els gimnasos i així tenir tots els noms en una sola petició
    $.getJSON("../php/controller/obtenirGimnasos.php", function (result) {
        
        
        if (result.length == 0) {
            $(".dades").append("<p>No hi ha cap gimnàs.</p>");
        }
         $.each(result, function (i, field) {   
            idButton = field.id;
            text = "<button class='minimal-indent'>&nbsp;&nbsp;&nbsp;Gimnàs '"+field.nom+"'. Direcció: "+field.direccio+'"&nbsp;&nbsp;&nbsp;</button><button type="button" id="'+idButton+'" class="btn btn-primary" onClick="clickEditar(this.id)">Editar</button></br>'
            $(".dades").append(text);
        });
        
    }); 
    
    $("#panelReserves").click(function() {
        window.location.href = 'reserves.html';
    });
                              
    $("#panelDadesCompte").click(function() {
        window.location.href = 'panelControl.html';
    });
    
    $("#crearGimnas").click(function() {
        window.location.href = 'crearGimnas.html';
    });
    
    $("#editarGimnas").click(function() {
        window.location.href = 'editarGimnas.html';
    });
    
});

function clickEditar(id) {
    
     // Es guarda en una cookie temporal quin es el gimnas
    document.cookie="gimnasEditar="+id+"; expires=0; path=/";
        
    // Es va a la pàgina de la reserva
    window.location.href = "editarGimnas2.html";
}
                  
function mostrarNavbarCorrecte() {

     // Depenen de si estem loguegats o no mostrem unes opcions o altres al navbar
    var nomUser = getCookie("usuariActual");
    if ( nomUser == "") {
        // No hi ha cap usuari loguegat
        $("#navbarDropdownUser").remove();
  
    } else {
        // Usuari loguegat
        //$("#navbarOpcions").children().length;
        $("#navbarSignUp").remove();
        $("#navbarLogin").remove();
        $("#navbarDropdownUserNom").html(nomUser+" <span class='caret'></span>");
        
    }
}

function getUser() {
    // Funció que demana les dadel del usuari actual al servidor
    var parametres = {
        usuariID: getCookie("usuariActual")
    };
    
    var post = $.post("../php/controller/obtenirUsuari.php", parametres, respostaObtenirUsuari, 'json');
}

function respostaObtenirUsuari(r) {
    rol = r.rol;
    
    // Mostrar les opcions d'administrador si el usuari te rol d'administrador
    if (rol == 'administrador') {
        $("#crearGimnas").css({"visibility":"visible"});
        $("#editarGimnas").css({"visibility":"visible"});
    }
}

// Funció que mostra si una cookie està creada o no (agafada de W3School)
function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1);
		if (c.indexOf(name) != -1) {
			return c.substring(name.length,c.length);
		}
	}
	return "";
}
