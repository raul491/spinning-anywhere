<?php

	require_once("../model/class.Reserva.php");
	require_once("../persistencia/class.ReservaPersistencia.php");

	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		// Agafem les dades del usuari (la data i la hora la passa el usuari, ja que podria estar en diferent zona horaria que el servidor)
		$user = $_POST['usuariID'];
		$dia = $_POST['dia'];
		$hora = $_POST['hora'];
		
		// Creem el objecte ReservaPersistencia i demanem les reserves del usuari que siguin posteriors a la data i hora actual
		$reservaPersistencia = new ReservaPersistencia();
		$reserves = $reservaPersistencia->getReservesUsuari($user,$dia,$hora);
		
		echo json_encode($reserves);

	}

?>
