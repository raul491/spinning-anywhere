<?php

	require_once("../model/class.Gimnas.php");
	require_once("../persistencia/class.GimnasPersistencia.php");
	
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		// Agafem les dades del usuari
		$nom = $_POST['nom'];
		$latitud = $_POST['latitud'];
		$longitud = $_POST['longitud'];
		$direccio = $_POST['direccio'];
		$horari = $_POST['horari'];
		$minutsEntreSetmana = $_POST['minutsEntreSetmana'];
		$horesEntreSetmana = $_POST['horesEntreSetmana'];
		$minutsCapSetmana = $_POST['minutsCapSetmana'];
		$horesCapSetmana = $_POST['horesCapSetmana'];
		$paginaReserva = $_POST['paginaReserva'];
		$llocsDisponibles = $_POST['llocsDisponibles'];


		$resposta = array();
		
		// Creem un objecte gimnas amb les dades pertinents
		$gimnas = new Gimnas();
		$gimnas->set_nom($nom);
		$gimnas->set_latitud($latitud);
		$gimnas->set_longitud($longitud);
		$gimnas->set_direccio($direccio);
		$gimnas->set_horari($horari);
		$gimnas->set_duracioReservesEntreSetmana($minutsEntreSetmana);
		$gimnas->set_horesPermesesReservarEntreSetmana($horesEntreSetmana);
		$gimnas->set_duracioReservesCapSetmana($minutsCapSetmana);
		$gimnas->set_horesPermesesReservarCapSetmana($horesCapSetmana);
		$gimnas->set_paginaReserva($paginaReserva);
		$gimnas->set_llocsDisponibles($llocsDisponibles);
		
		// Guardem aquest gimnas en la base de dates
		$gimnasPersistencia = new GimnasPersistencia();
		$respostaPersistencia = $gimnasPersistencia->crearGimnas($gimnas);
		
		
		switch ($respostaPersistencia) {
			case 0:
				$resposta['error'] = 0;
				break;
			case 1:
				$resposta['error'] = 1;
				$resposta['missatgeError'] = "Error al crear el gimnàs";
				break;
			case 2:
				$resposta['error'] = 1;
				$resposta['missatgeError'] = "Error";
				break;
		}
		
		echo json_encode($resposta);
		
	}

?>
