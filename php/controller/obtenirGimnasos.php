<?php

	require_once("../model/class.Gimnas.php");
	require_once("../persistencia/class.GimnasPersistencia.php");


	// Creem el objecte GimnasPersistencia i demanem la llista de gimnasos que hi ha a la base de dades
	// Ha de retornar una llista amb els diferents objectes Gimnas
	$gimnasPersistencia = new GimnasPersistencia();
	$llistaGimnasos = $gimnasPersistencia->getLlistaGimnasos();

	// Com el objecte Gimnas implementa JsonSerializable es pot pasar directament en un JSON
	echo json_encode($llistaGimnasos);


?>
