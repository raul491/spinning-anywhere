<?php

	require_once("../model/class.Gimnas.php");
	require_once("../persistencia/class.GimnasPersistencia.php");

	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		// Agafem les dades del usuari
		$gimnasID = $_POST['gimnasID'];
		
		// Creem el objecte GimnasPersistencia i demanem les dades d'un gimnas
		$gimnasPersistencia = new GimnasPersistencia();
		$gimnas = $gimnasPersistencia->getGimnas($gimnasID);

		// Com el objecte Gimnas implementa JsonSerializable es pot pasar directament en un JSON
		echo json_encode($gimnas);

	}

?>
