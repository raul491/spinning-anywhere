<?php

	require_once("../model/class.Usuari.php");
	require_once("../persistencia/class.UsuariPersistencia.php");
	
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		// Agafem les dades del usuari
		$nom = $_POST['usuariID'];
		$passAntiga = $_POST['passAntiga'];
		$passNova = $_POST['passNova'];

		$resposta = array();
		
		// Primer es comproba que la contrasenya antiga sigui la correcta
		// Creem un objecte usuari amb les dades del login
		$usuari = new Usuari($nom,$passAntiga,''); // El mail no es fa servir en aquest cas
		$usuariPersistencia = new UsuariPersistencia();
		$respostaPersistencia = $usuariPersistencia->consultarPassUsuari($usuari);
		
		if ($respostaPersistencia != 0) {
			$resposta['error'] = 1;
			$resposta['missatgeError'] = "Contrasenya incorrecte";
			echo json_encode($resposta);
			return;
		}
		
		// Si la contrasenya antiga es correcte es canvia la contrasenya per la nova
		$respostaPersistencia = $usuariPersistencia->canviarPass($nom,$passNova);
		if ($respostaPersistencia == 0) {
			$resposta['error'] = 0;
		} else {
			$resposta['error'] = 1;
			$resposta['missatgeError'] = "Error al canviar la contrasenya";
		}
		
		echo json_encode($resposta);
		
	}

?>
