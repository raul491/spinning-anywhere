<?php

	require_once("../model/class.Usuari.php");
	require_once("../persistencia/class.UsuariPersistencia.php");
	
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		// Agafem les dades del usuari
		$nom = $_POST['usuari'];
		$pass = $_POST['pass'];
		
		$resposta = array();
		
		// Creem un objecte usuari amb les dades del login
		$usuari = new Usuari($nom,$pass,''); // El mail no es fa servir en aquest cas
		
		// Consultem si esta aquest usuari en la base de dates amb la contrasenya correcte
		$usuariPersistencia = new UsuariPersistencia();
		$respostaPersistencia = $usuariPersistencia->consultarPassUsuari($usuari);
		
		switch ($respostaPersistencia) {
			case 0:
				$resposta['error'] = 0;
				// Ens logueguem
				setcookie('usuariActual',$nom, 0, "/"); // Expira al acabar la sessió
				break;
			case 1:
				$resposta['error'] = 1;
				$resposta['missatgeError'] = "Validació incorrecte";
				break;
			case 2:
				$resposta['error'] = 1;
				$resposta['missatgeError'] = "Error";
				break;
		}
		
		echo json_encode($resposta);
	}

?>
