<?php

	require_once("../model/class.Usuari.php");
	require_once("../persistencia/class.UsuariPersistencia.php");
	require_once("../persistencia/class.ReservaPersistencia.php");
	
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		// Agafem les dades del usuari
		$nom = $_POST['usuariID'];

		$resposta = array();
		
		// Creem un objecte usuari
		// Nomes ens interessa el nom, però creem un objecte usuari perquè en persistencia es borra un usuari quan se li passa un objecte Usuari
		$usuari = new Usuari($nom,'',''); 
		
		// Primer triem les seves reserves
		$reservaPersistencia = new ReservaPersistencia();
		$reservaPersistencia->eliminarReservesUsuari($usuari);
		
		// I després esborrem el usuari
		$usuariPersistencia = new UsuariPersistencia();
		$respostaPersistencia = $usuariPersistencia->eliminarUsuari($usuari);
		
		// Esborrem la cookie del usuari actiu
		setcookie('usuariActual',$nom, time()-3600, "/");
		
		// Tal com està implementat el codi no es té previst que falli
		$resposta['error'] = 0;
		echo json_encode($resposta);
		
	}

?>
