<?php

	require_once("../model/class.Usuari.php");
	require_once("../persistencia/class.UsuariPersistencia.php");
	
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		// Agafem les dades del usuari
		$nom = $_POST['usuari'];
		$pass = $_POST['pass'];
		$pass2 = $_POST['pass2'];
		$mail = $_POST['mail'];

		$resposta = array();
		
		// Creem un objecte usuari
		$usuari = new Usuari($nom,$pass,$mail);
		
		// Guardem aquest usuari en la base de dates
		$usuariPersistencia = new UsuariPersistencia();
		$respostaPersistencia = $usuariPersistencia->guardarUsuari($usuari);
		
		
		switch ($respostaPersistencia) {
			case 0:
				$resposta['error'] = 0;
				// Si s'ha pogut crear l'usuari el guardem en una cookie
				setcookie('usuariActual',$nom, 0, "/"); // Expira al acabar la sessió
				break;
			case 1:
				$resposta['error'] = 1;
				$resposta['missatgeError'] = "Nom d'usuari ja existent";
				break;
			case 2:
				$resposta['error'] = 1;
				$resposta['missatgeError'] = "Error";
				break;
		}
		
		echo json_encode($resposta);
		
	}

?>
