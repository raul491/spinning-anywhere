<?php

	require_once("../model/class.Reserva.php");
	require_once("../persistencia/class.ReservaPersistencia.php");

	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		// Agafem les dades del usuari
		$gimnasID = $_POST['gimnasID'];
		$dia = $_POST['data'];
		$hora = $_POST['hora'];
		
		// Creem el objecte ReservaPersistencia i demanem les bicis ocupades en aquesta classe
		$reservaPersistencia = new ReservaPersistencia();
		$bicis = $reservaPersistencia->getBicisClasse($gimnasID, $dia, $hora);
		

		// Es retorna un array amb els números de les bicis ocupades de la classe demanada
		// No hi ha taula bici perquè en el meu diagrama no fa falta, la reserva fa la funció de bici
		echo json_encode($bicis);

	}

?>
