<?php

	require_once("../model/class.Reserva.php");
	require_once("../persistencia/class.ReservaPersistencia.php");
	
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		// Agafem les dades de la vista
		$gimnasID = $_POST['gimnasID'];
		$usuari = $_POST['usuariID'];
		$dia = $_POST['dia'];
		$hora = $_POST['hora'];

		// Creem el objecte ReservaPersistencia
		$reservaPersistencia = new ReservaPersistencia();
		
		// Es crea la reserva amb les dades del usuari
		$reserva = new Reserva($gimnasID, $usuari, $dia, $hora, ''); // La bici no fa falta
		
		// S'elimina la reserva
		$respostaPersistencia = $reservaPersistencia->eliminarReserva($reserva);
		
		$resposta = array();
		
		if ($respostaPersistencia) {
			$resposta['error'] = 0;
		} else {
			$resposta['error'] = 1;
		}
		
		echo json_encode($resposta);
}

?>
