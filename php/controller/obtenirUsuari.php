<?php

	require_once("../model/class.Usuari.php");
	require_once("../persistencia/class.UsuariPersistencia.php");
	
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		// Agafem les dades del usuari
		$nom = $_POST['usuariID'];
		
		// Creem el objecte UsuariPersistencia
		$usuariPersistencia = new UsuariPersistencia();
		
		// Demanem les dades del usuari a patir del seu id (retorna un objecte Usuari)
		$usuari = $usuariPersistencia->obtenirUsuari($nom);
		
		// El usuari es pot pasar per JSON per que implementa JsonSerializable
		echo json_encode($usuari);
		
	}

?>
