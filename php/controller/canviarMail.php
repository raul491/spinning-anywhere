<?php

	require_once("../model/class.Usuari.php");
	require_once("../persistencia/class.UsuariPersistencia.php");
	
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		// Agafem les dades del usuari
		$nom = $_POST['usuariID'];
		$mail = $_POST['mail'];

		$resposta = array();
		
		$usuariPersistencia = new UsuariPersistencia();
		$respostaPersistencia = $usuariPersistencia->canviarMail($nom,$mail);
		
		
		switch ($respostaPersistencia) {
			case 0:
				$resposta['error'] = 0;
				break;
			case 1:
				$resposta['error'] = 1;
				$resposta['missatgeError'] = "Error al actualitzar el mail";
				break;
			case 2:
				$resposta['error'] = 1;
				$resposta['missatgeError'] = "Error";
				break;
		}
		
		echo json_encode($resposta);
		
	}

?>
