<?php

	require_once("../model/class.Reserva.php");
	require_once("../persistencia/class.ReservaPersistencia.php");
	
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		// Agafem les dades de la vista
		$gimnasID = $_POST['gimnasID'];
		$usuari = $_POST['usuariID'];
		$dia = $_POST['data'];
		$hora = $_POST['hora'];
		$bici = $_POST['bici'];
		
		$resposta = array();
		
		// Creem el objecte ReservaPersistencia
		$reservaPersistencia = new ReservaPersistencia();
		
		// Es crea la reserva amb les dades del usuari
		$reserva = new Reserva($gimnasID, $usuari, $dia, $hora, $bici);
		
		// Es comproba que el usuari no tingui ja una reserva en el mateix gimnas, el mateix dia i la mateixa hora
		if ($reservaPersistencia->reservaJaFeta($reserva)) {
			$resposta['error'] = 1;
			$resposta['missatgeError'] = "Ja tenies aquesta hora reservada";
			echo json_encode($resposta);
			return;
		}
		
		// Es fa la reserva
		if ($reservaPersistencia->reservar($reserva)) {
			// Rerserva correcte
			$resposta['error'] = 0;
		} else {
			// Reserva incorrecte
			$resposta['error'] = 1;
			$resposta['missatgeError'] = "Error";
		}
		
		// Pasem la resposta en un objecte JSON
		echo json_encode($resposta);
	
	}

?>
