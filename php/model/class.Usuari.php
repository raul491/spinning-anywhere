<?php

class Usuari implements JsonSerializable {

    private $_nom;
    private $_passwd;
    private $_mail;
    private $_gym_preferits;
    private $_rol = 'usuari'; // Posem un rol per defecte;
    
    function get_nom() {
        return $this->_nom;
    }

    function get_passwd() {
        return $this->_passwd;
    }

    function get_mail() {
        return $this->_mail;
    }

    function get_gym_preferits() {
        return $this->_gym_preferits;
    }
    
    function get_rol() {
		return $this->_rol;
	}

    function set_nom($_nom) {
        $this->_nom = $_nom;
    }

    function set_passwd($_passwd) {
        $this->_passwd = $_passwd;
    }

    function set_mail($_mail) {
        $this->_mail = $_mail;
    }

    function set_gym_preferits($_gym_preferits) {
        $this->_gym_preferits = $_gym_preferits;
    }
    
    function set_rol($_rol) {
		$this->_rol = $_rol;
	}

    function __construct($_nom, $_passwd, $_mail) {
        $this->_nom = $_nom;
        $this->_passwd = $_passwd;
        $this->_mail = $_mail;
    }
    
    public function jsonSerialize() {
        return [
            'nom' => $this->_nom,
            'passwd' => $this->_passwd,
            'mail' => $this->_mail,
            'gym_preferits' => $this->_gym_preferits,
            'rol' => $this->_rol
        ];
    }
    
}

?>
