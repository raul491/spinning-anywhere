<?php

class Reserva implements JsonSerializable {

	private $_gimnasID;
	private $_usuari;
	private $_dia;
	private $_hora;
	private $_bici;
        
    function get_gimnasID() {
        return $this->_gimnasID;
    }

    function get_usuari() {
        return $this->_usuari;
    }

    function get_dia() {
        return $this->_dia;
    }

    function get_hora() {
        return $this->_hora;
    }

    function get_bici() {
        return $this->_bici;
    }

    function set_gimnasID($_gimnasID) {
        $this->_gimnasID = $_gimnasID;
    }

    function set_usuari($_usuari) {
        $this->_usuari = $_usuari;
    }

    function set_dia($_dia) {
        $this->_dia = $_dia;
    }

    function set_hora($_hora) {
        $this->_hora = $_hora;
    }

    function set_bici($_bici) {
        $this->_bici = $_bici;
    }

    function __construct($_gimnasID, $_usuari, $_dia, $_hora, $_bici) {
        $this->_gimnasID = $_gimnasID;
        $this->_usuari = $_usuari;
        $this->_dia = $_dia;
        $this->_hora = $_hora;
        $this->_bici = $_bici;
    }
    
    public function jsonSerialize() {
        return [
            'gimnasid' => $this->_gimnasID,
            'usuari' => $this->_usuari,
            'dia' => $this->_dia,
            'hora' => $this->_hora,
            'bici' => $this->_bici
        ];
	}
	

}
?>

