<?php

class Gimnas implements JsonSerializable {

    private $_id;
    private $_nom;
    private $_latitud;
    private $_longitud;
    private $_direccio;
    private $_horari;
    private $_llocsDisponibles;
    private $_duracioReservesEntreSetmana;
    private $_horesPermesesReservarEntreSetmana;
    private $_duracioReservesCapSetmana;
    private $_horesPermesesReservarCapSetmana;
    private $_paginaReserva;
    
    function get_id() {
        return $this->_id;
    }

    function get_nom() {
        return $this->_nom;
    }

    function get_latitud() {
        return $this->_latitud;
    }

    function get_longitud() {
        return $this->_longitud;
    }

    function get_direccio() {
        return $this->_direccio;
    }

    function get_horari() {
        return $this->_horari;
    }

    function get_llocsDisponibles() {
        return $this->_llocsDisponibles;
    }

    function get_duracioReservesEntreSetmana() {
        return $this->_duracioReservesEntreSetmana;
    }

    function get_horesPermesesReservarEntreSetmana() {
        return $this->_horesPermesesReservarEntreSetmana;
    }

    function get_duracioReservesCapSetmana() {
        return $this->_duracioReservesCapSetmana;
    }

    function get_horesPermesesReservarCapSetmana() {
        return $this->_horesPermesesReservarCapSetmana;
    }
    
    function get_paginaReserva() {
        return $this->_paginaReserva;
    }

    function set_id($_id) {
        $this->_id = $_id;
    }

    function set_nom($_nom) {
        $this->_nom = $_nom;
    }

    function set_latitud($_latitud) {
        $this->_latitud = $_latitud;
    }

    function set_longitud($_longitud) {
        $this->_longitud = $_longitud;
    }

    function set_direccio($_direccio) {
        $this->_direccio = $_direccio;
    }

    function set_horari($_horari) {
        $this->_horari = $_horari;
    }

    function set_llocsDisponibles($_llocsDisponibles) {
        $this->_llocsDisponibles = $_llocsDisponibles;
    }

    function set_duracioReservesEntreSetmana($_duracioReservesEntreSetmana) {
        $this->_duracioReservesEntreSetmana = $_duracioReservesEntreSetmana;
    }

    function set_horesPermesesReservarEntreSetmana($_horesPermesesReservarEntreSetmana) {
        $this->_horesPermesesReservarEntreSetmana = $_horesPermesesReservarEntreSetmana;
    }

    function set_duracioReservesCapSetmana($_duracioReservesCapSetmana) {
        $this->_duracioReservesCapSetmana = $_duracioReservesCapSetmana;
    }

    function set_horesPermesesReservarCapSetmana($_horesPermesesReservarCapSetmana) {
        $this->_horesPermesesReservarCapSetmana = $_horesPermesesReservarCapSetmana;
    }
    
    function set_paginaReserva($_paginaReserva) {
        $this->_paginaReserva = $_paginaReserva;
    }

    function __construct() {
        
    }
    
     public function jsonSerialize() {
        return [
            'id' => $this->_id,
            'nom' => $this->_nom,
            'latitud' => $this->_latitud,
            'longitud' => $this->_longitud,
            'direccio' => $this->_direccio,
            'horari' => $this->_horari,
            'llocsdisponibles' => $this->_llocsDisponibles,
            'duracioreservesentresetmana' => $this->_duracioReservesEntreSetmana,
            'horespermesesreservarentresetmana' => $this->_horesPermesesReservarEntreSetmana,
            'duracioreservescapsetmana' => $this->_duracioReservesCapSetmana,
            'horespermesesreservarcapsetmana' => $this->_horesPermesesReservarCapSetmana,
            'paginareserva' => $this->_paginaReserva
        ];
    }

}
?>

