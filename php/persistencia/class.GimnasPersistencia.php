<?php

require_once("../model/class.Gimnas.php");

class GimnasPersistencia {
	
	public function getLlistaGimnasos() {
		
		$strDSN = "pgsql:dbname=spinning_anywhere;host=localhost;port=5432;user=chaptersix;password=chaptersix";
        try {
            $objPDO = new PDO($strDSN);
        } catch (PDOException $e) {
            // Problemes en la connexió de la base de dades
        }
        
        // Guardem tots els gimnasos en un array
        $llista = array();
		
		$select = "SELECT * FROM gimnas;";
        $objStatement = $objPDO->prepare($select);
        $objStatement->execute();

        $id;
        
        while ($arRow = $objStatement->fetch(PDO::FETCH_ASSOC)) {
			
			// Guardem cada resposta en un objecte Gimnas
			$gimnas = new Gimnas();
            foreach ($arRow as $key => $value) {
				
				if ($key == 'id') {
					$gimnas->set_id($value);
				} else if ($key == 'nom') {
					$gimnas->set_nom($value);
				} else if ($key == 'latitud') {
					$gimnas->set_latitud($value);
				} else if ($key == 'longitud') {
					$gimnas->set_longitud($value);
				} else if ($key == 'direccio') {
					$gimnas->set_direccio($value);
				} else if ($key == 'horari') {
					$gimnas->set_horari($value);
				} else if ($key == 'llocsdisponibles') {
					$gimnas->set_llocsDisponibles($value);
				} else if ($key == 'duracioreservesrntresetmana') {
					$gimnas->set_duracioReservesEntreSetmana($value);
				} else if ($key == 'horespermesesreservarentresetmana') {
					$gimnas->set_horesPermesesReservarEntreSetmana($value);
				} else if ($key == 'duracioreservescapsetmana') {
					$gimnas->set_duracioReservesCapSetmana($value);
				} else if ($key == 'horespermesesreservarcapsetmana') {
					$gimnas->set_horesPermesesReservarCapSetmana($value);
				} else if ($key == 'paginareserva') {
					$gimnas->set_paginaReserva($value);
				}
            }
            
            $llista[] = $gimnas;
        }
        
        $objPDO = NULL;
		return $llista;
		
	}
	
	public function getGimnas($gimnasID) {
		$strDSN = "pgsql:dbname=spinning_anywhere;host=localhost;port=5432;user=chaptersix;password=chaptersix";
        try {
            $objPDO = new PDO($strDSN);
        } catch (PDOException $e) {
            // Problemes en la connexió de la base de dades
        }
		
		$select = "SELECT * FROM gimnas where id = '".$gimnasID."';";
        $objStatement = $objPDO->prepare($select);
        $objStatement->execute();
        
        while ($arRow = $objStatement->fetch(PDO::FETCH_ASSOC)) {
			// Guardem la resposta en un objecte Gimnas (nomes hi haurà una resposta)
			$gimnas = new Gimnas();
            foreach ($arRow as $key => $value) {
				
				if ($key == 'id') {
					$gimnas->set_id($value);
				} else if ($key == 'nom') {
					$gimnas->set_nom($value);
				} else if ($key == 'latitud') {
					$gimnas->set_latitud($value);
				} else if ($key == 'longitud') {
					$gimnas->set_longitud($value);
				} else if ($key == 'direccio') {
					$gimnas->set_direccio($value);
				} else if ($key == 'horari') {
					$gimnas->set_horari($value);
				} else if ($key == 'llocsdisponibles') {
					$gimnas->set_llocsDisponibles($value);
				} else if ($key == 'duracioreservesentresetmana') {
					$gimnas->set_duracioReservesEntreSetmana($value);
				} else if ($key == 'horespermesesreservarentresetmana') {
					$gimnas->set_horesPermesesReservarEntreSetmana($value);
				} else if ($key == 'duracioreservescapsetmana') {
					$gimnas->set_duracioReservesCapSetmana($value);
				} else if ($key == 'horespermesesreservarcapsetmana') {
					$gimnas->set_horesPermesesReservarCapSetmana($value);
				} else if ($key == 'paginareserva') {
					$gimnas->set_paginaReserva($value);
				}
            }

        }
        
        $objPDO = NULL;
		return $gimnas;
		
		
	}
	
	public function crearGimnas($gimnas) {
        $strDSN = "pgsql:dbname=spinning_anywhere;host=localhost;port=5432;user=chaptersix;password=chaptersix";
        
        $resposta = array();
        
        try {
            $objPDO = new PDO($strDSN);
        } catch (PDOException $e) {
            // Problemes en la connexió de la base de dades
            return 2;
        }
        
        $id = $this->nouId();
        //return $gimnas->get_nom;
        $insert = "INSERT INTO gimnas(id,nom,latitud,longitud,direccio,horari,llocsDisponibles,duracioReservesEntreSetmana,horesPermesesReservarEntreSetmana, duracioReservesCapSetmana, horesPermesesReservarCapSetmana, paginaReserva) VALUES('".$id."','".$gimnas->get_nom()."','".$gimnas->get_latitud()."','".$gimnas->get_longitud()."','".$gimnas->get_direccio()."','".$gimnas->get_horari()."','".$gimnas->get_llocsDisponibles()."','".$gimnas->get_duracioReservesEntreSetmana()."','".$gimnas->get_horesPermesesReservarEntreSetmana()."','".$gimnas->get_duracioReservesCapSetmana()."','".$gimnas->get_horesPermesesReservarCapSetmana()."','".$gimnas->get_paginaReserva()."');";
        $objStatement = $objPDO->prepare($insert);
        if ($objStatement->execute()) {
            // Alta correcte
            $objPDO = NULL;
            return 0;
        } else {
            // Alta incorrecte
            $objPDO = NULL;
            return 1;
        }
        
    }
    
    public function nouId() {
		// Funció que mira quien es el últim id per posar un nou id al nou gimnas
		$strDSN = "pgsql:dbname=spinning_anywhere;host=localhost;port=5432;user=chaptersix;password=chaptersix";
        try {
            $objPDO = new PDO($strDSN);
        } catch (PDOException $e) {
            // Problemes en la connexió de la base de dades
        }
		
		$select = "SELECT id FROM gimnas ORDER BY id desc;";
        $objStatement = $objPDO->prepare($select);
        $objStatement->execute();
        $id = 0;
		
        while ($arRow = $objStatement->fetch(PDO::FETCH_ASSOC)) {
            foreach ($arRow as $key => $value) {
				if ($key == 'id') {
					$id = $value;
				}
            }
			break;
        }
        
        $objPDO = NULL;
		return $id+1;
		
	}
	
	public function editarGimnas($gimnas) {
        $strDSN = "pgsql:dbname=spinning_anywhere;host=localhost;port=5432;user=chaptersix;password=chaptersix";
        
        $resposta = array();
        
        try {
            $objPDO = new PDO($strDSN);
        } catch (PDOException $e) {
            // Problemes en la connexió de la base de dades
            return 2;
        }
        
        $update = "UPDATE gimnas SET (nom,latitud,longitud,direccio,horari,llocsDisponibles,duracioReservesEntreSetmana,horesPermesesReservarEntreSetmana, duracioReservesCapSetmana, horesPermesesReservarCapSetmana, paginaReserva) = ('".$gimnas->get_nom()."','".$gimnas->get_latitud()."','".$gimnas->get_longitud()."','".$gimnas->get_direccio()."','".$gimnas->get_horari()."','".$gimnas->get_llocsDisponibles()."','".$gimnas->get_duracioReservesEntreSetmana()."','".$gimnas->get_horesPermesesReservarEntreSetmana()."','".$gimnas->get_duracioReservesCapSetmana()."','".$gimnas->get_horesPermesesReservarCapSetmana()."','".$gimnas->get_paginaReserva()."') WHERE id = '" . $gimnas->get_id() . "';";

		$objStatement = $objPDO->prepare($update);
        if ($objStatement->execute()) {
            // Gimnas actualitzat correctament
            $objPDO = NULL;
            return 0;
        } else {
            // Error al actualitzar el gimnas
            $objPDO = NULL;
            return 1;
        }
        
    }
	
}
