<?php

require_once("../model/class.Reserva.php");

class ReservaPersistencia {
	
	public function reservaJaFeta($reserva) {
		// Es comproba que l'usuari no hagi ja reservat en la mateixa hora en el mateix gimnàs
		
		$strDSN = "pgsql:dbname=spinning_anywhere;host=localhost;port=5432;user=chaptersix;password=chaptersix";
        try {
            $objPDO = new PDO($strDSN);
        } catch (PDOException $e) {
            // Problemes en la connexió de la base de dades
            return false;
        }

        $select = "SELECT * FROM reserves WHERE (gimnasID,usuariID,dia,hora) = ('".$reserva->get_gimnasID()."','".$reserva->get_usuari()."','".$reserva->get_dia()."','".$reserva->get_hora()."');";
        
        $i = 0;
        $objStatement = $objPDO->prepare($select);
        $objStatement->execute();
        while ($arRow = $objStatement->fetch(PDO::FETCH_ASSOC)) {
            $i++;
        };

        if ($i != 0) {
            // Reserva ja feta
            $objPDO = NULL;
			return true;
        } else {
			// Reserva no feta
            $objPDO = NULL;
			return false;
		}
        
	}
	
	public function reservar($reserva) {
		$strDSN = "pgsql:dbname=spinning_anywhere;host=localhost;port=5432;user=chaptersix;password=chaptersix";
        try {
            $objPDO = new PDO($strDSN);
        } catch (PDOException $e) {
            // Problemes en la connexió de la base de dades
        }

		$insert = "INSERT INTO reserves(gimnasID,usuariID,dia,hora,bici) VALUES('".$reserva->get_gimnasID()."','".$reserva->get_usuari()."','".$reserva->get_dia()."','".$reserva->get_hora()."','".$reserva->get_bici()."');";
        $objStatement = $objPDO->prepare($insert);
        if ($objStatement->execute()) {
            // Alta correcte
            $objPDO = NULL;
            return true;
        } else {
			// Alta incorrecte
			$objPDO = NULL;
            return false;
        }
	}
	
	public function getBicisClasse($gimnasID, $dia, $hora) {
		// Funció que donat un gimnàs, un dia y una hora retorna els números de les bicis ocupades de la classe
		$strDSN = "pgsql:dbname=spinning_anywhere;host=localhost;port=5432;user=chaptersix;password=chaptersix";
        try {
            $objPDO = new PDO($strDSN);
        } catch (PDOException $e) {
            // Problemes en la connexió de la base de dades
        }
        
        $bicis = array();

		$select = "SELECT bici FROM reserves WHERE (gimnasID,dia,hora) = ('".$gimnasID."','".$dia."','".$hora."');";
		
        $objStatement = $objPDO->prepare($select);
        $objStatement->execute();
        while ($arRow = $objStatement->fetch(PDO::FETCH_ASSOC)) {
            foreach ($arRow as $key => $value) {
				$bicis[] = $value;
			};
        };
		
		$objPDO = NULL;
		return $bicis;
	}
	
	public function getReservesUsuari($user,$dia,$hora){
		// Funció que donat un usuari, un dia i una hora retorna la llista de reserves feta per aquest usuari desde el dia actual
		$strDSN = "pgsql:dbname=spinning_anywhere;host=localhost;port=5432;user=chaptersix;password=chaptersix";
        try {
            $objPDO = new PDO($strDSN);
        } catch (PDOException $e) {
            // Problemes en la connexió de la base de dades
        }
        
        $reserves = array();
        $select = "SELECT * FROM reserves WHERE usuariID = '".$user."' AND dia >= '".$dia."' ORDER BY dia asc, hora asc;";
        
		$objStatement = $objPDO->prepare($select);
        $objStatement->execute();
        while ($arRow = $objStatement->fetch(PDO::FETCH_ASSOC)) {
			
			// Guardem cada resposta en un objecte Reserva
            foreach ($arRow as $key => $value) {
				if ($key == 'gimnasid') {
					$reservaGimnas = $value;
				} else if ($key == 'usuariid') {
					$reservaUsuari = $value;
				} else if ($key == 'dia') {
					$reservaDia = $value;
				} else if ($key == 'hora') {
					$reservaHora = $value;
				} else if ($key == 'bici') {
					$reservaBici = $value;
				}
				$reserva = new Reserva($reservaGimnas, $reservaUsuari, $reservaDia, $reservaHora, $reservaBici);
            }
            
            $reserves[] = $reserva;
        }
		
		$objPDO = NULL;
		return $reserves;
	}
	
	public function eliminarReserva($reserva)  {
		$strDSN = "pgsql:dbname=spinning_anywhere;host=localhost;port=5432;user=chaptersix;password=chaptersix";
        try {
            $objPDO = new PDO($strDSN);
        } catch (PDOException $e) {
            // Problemes en la connexió de la base de dades
        }
        
        
        // Comprobem que la reserva existeix abans de eliminar
        $select = "SELECT * FROM reserves WHERE (gimnasID,usuariID,dia,hora) = ('".$reserva->get_gimnasID()."','".$reserva->get_usuari()."','".$reserva->get_dia()."','".$reserva->get_hora()."');";
        $i = 0;
        $objStatement = $objPDO->prepare($select);
        $objStatement->execute();
        while ($arRow = $objStatement->fetch(PDO::FETCH_ASSOC)) {
            $i++;
        };

        if ($i == 0) {
            // Reserva no existent
            $objPDO = NULL;
            return false;
        }

        // Esborrar la reserva
        $delete = "DELETE FROM reserves WHERE (gimnasID,usuariID,dia,hora) = ('".$reserva->get_gimnasID()."','".$reserva->get_usuari()."','".$reserva->get_dia()."','".$reserva->get_hora()."');";
        $objStatement = $objPDO->prepare($delete);
        // Si un delete elimina una reserva que no existeix no falla, per tant hem tingut que fer un select abans per comprobar que la reserva existeix
        $objStatement->execute();
        // Baixa correcte
        $objPDO = NULL;
        return true;
		
	}
	
	public function eliminarReservesUsuari($usuari) {
		$strDSN = "pgsql:dbname=spinning_anywhere;host=localhost;port=5432;user=chaptersix;password=chaptersix";
        try {
            $objPDO = new PDO($strDSN);
        } catch (PDOException $e) {
            // Problemes en la connexió de la base de dades
        }
        // Esborrar les reserves del usuari passat
        $delete = "DELETE FROM reserves WHERE (usuariID) = ('".$usuari->get_nom()."');";
        $objStatement = $objPDO->prepare($delete);
        $objStatement->execute();
        $objPDO = NULL;
        return true;
	}
	
}
