<?php

require_once("../model/class.Usuari.php");

class UsuariPersistencia {

	public function guardarUsuari($usuari) {
        $strDSN = "pgsql:dbname=spinning_anywhere;host=localhost;port=5432;user=chaptersix;password=chaptersix";
        
        $resposta = array();
        
        try {
            $objPDO = new PDO($strDSN);
        } catch (PDOException $e) {
            // Problemes en la connexió de la base de dades
            return 2;
        }
        $insert = "INSERT INTO usuari(nom,pass,mail,rol) VALUES('" . $usuari->get_nom() . "','" . $usuari->get_passwd() . "','" . $usuari->get_mail() . "','" . $usuari->get_rol() . "');";
        $objStatement = $objPDO->prepare($insert);
        if ($objStatement->execute()) {
            // Alta correcte
            $objPDO = NULL;
            return 0;
        } else {
            // Persona ja existent
            $objPDO = NULL;
            return 1;
        }
        
    }
    
    public function consultarPassUsuari($usuari) {
		// Funció que donat un usuari mira si està aquest usuari en la base de dates i comproba que la seva contrasenya es la correcte
		$strDSN = "pgsql:dbname=spinning_anywhere;host=localhost;port=5432;user=chaptersix;password=chaptersix";
        
        try {
            $objPDO = new PDO($strDSN);
        } catch (PDOException $e) {
            // Problemes en la connexió de la base de dades
            return 2;
        }

        $select = "SELECT pass FROM usuari WHERE nom = '" . $usuari->get_nom() . "';";

        $i = 0;
        $objStatement = $objPDO->prepare($select);
        $objStatement->execute();
        while ($arRow = $objStatement->fetch(PDO::FETCH_ASSOC)) {
            foreach ($arRow as $key => $value) {
                if ($value == $usuari->get_passwd()) {
                    // Validació correcte
                    $objPDO = NULL;
                    return 0;
                } else {
                    // Validació incorrecte (contrasenya incorrecte)
                    $objPDO = NULL;
                    return 1;
                }
            };
            $i++;
        };

        if ($i == 0) {
            // Validació incorrecte (no es troba el usuari)
            $objPDO = NULL;
            return 1;
		}
		
	}
	
	public function obtenirUsuari($nom) {
		// Funció que donat un id d'un usuari retorna el usuari amb totes les seves dades
		$strDSN = "pgsql:dbname=spinning_anywhere;host=localhost;port=5432;user=chaptersix;password=chaptersix";
        
        try {
            $objPDO = new PDO($strDSN);
        } catch (PDOException $e) {
            // Problemes en la connexió de la base de dades
        }

        $select = "SELECT * FROM usuari WHERE nom = '" . $nom . "';";

		$usuari;

        $i = 0;
        $objStatement = $objPDO->prepare($select);
        $objStatement->execute();
        while ($arRow = $objStatement->fetch(PDO::FETCH_ASSOC)) {
            foreach ($arRow as $key => $value) {
				if ($key == 'nom') {
					$usuariNom = $value;
				} else if ($key == 'pass') {
					$usuariPass = $value;
				} else if ($key == 'mail') {
					$usuariMail = $value;
				} else if ($key == 'gymspreferits') {
					$usuariGymsPreferits = $value;
				} else if ($key == 'rol') {
					$rol = $value;
				}
				$usuari = new Usuari($usuariNom, $usuariPass, $usuariMail);
				$usuari->set_gym_preferits($usuariGymsPreferits);
				$usuari->set_rol($rol);
			}		
		}
		
		$objPDO = NULL;
		return $usuari;
	}
	
	public function canviarMail($nom, $mail) {
		// Funció que canvia el mail d'un usuari
		$strDSN = "pgsql:dbname=spinning_anywhere;host=localhost;port=5432;user=chaptersix;password=chaptersix";
        
        try {
            $objPDO = new PDO($strDSN);
        } catch (PDOException $e) {
            // Problemes en la connexió de la base de dades
            return 2;
        }


		$update = "UPDATE usuari SET mail = '" . $mail . "' WHERE nom = '" . $nom . "';";

		$objStatement = $objPDO->prepare($update);
        if ($objStatement->execute()) {
            // Mail actualitzat correctament
            $objPDO = NULL;
            return 0;
        } else {
            // Error al actualitzar el mail
            $objPDO = NULL;
            return 1;
        }
	}
	
	public function canviarPass($nom, $pass) {
		// Funció que canvia la contrasenya d'un usuari
		$strDSN = "pgsql:dbname=spinning_anywhere;host=localhost;port=5432;user=chaptersix;password=chaptersix";
        
        try {
            $objPDO = new PDO($strDSN);
        } catch (PDOException $e) {
            // Problemes en la connexió de la base de dades
            return 2;
        }


		$update = "UPDATE usuari SET pass = '" . $pass . "' WHERE nom = '" . $nom . "';";

		$objStatement = $objPDO->prepare($update);
        if ($objStatement->execute()) {
            // Pass actualitzat correctament
            $objPDO = NULL;
            return 0;
        } else {
            // Error al actualitzar el pass
            $objPDO = NULL;
            return 1;
        }
	}
	
	public function eliminarUsuari($usuari) {
		$strDSN = "pgsql:dbname=spinning_anywhere;host=localhost;port=5432;user=chaptersix;password=chaptersix";
        try {
            $objPDO = new PDO($strDSN);
        } catch (PDOException $e) {
            // Problemes en la connexió de la base de dades
        }
        // Esborrar el usuari
        $delete = "DELETE FROM usuari WHERE (nom) = ('".$usuari->get_nom()."');";
        $objStatement = $objPDO->prepare($delete);
        $objStatement->execute();
        $objPDO = NULL;
        return true;
	}
	
    
}

?>
