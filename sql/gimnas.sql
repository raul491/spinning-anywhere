CREATE DATABASE "spinning_anywhere";

\connect "spinning_anywhere";

CREATE TABLE gimnas(
id serial PRIMARY KEY,
nom character varying(40) NOT NULL,
latitud float NOT NULL,
longitud float NOT NULL,
direccio character varying(50),
horari character varying(50), 
llocsDisponibles int,
duracioReservesEntreSetmana int,
horesPermesesReservarEntreSetmana character varying(100),
duracioReservesCapSetmana int,
horesPermesesReservarCapSetmana character varying(100),
paginaReserva character varying(20)
);

GRANT ALL PRIVILEGES ON gimnas TO "chaptersix";


INSERT INTO gimnas (nom,latitud,longitud,direccio,horari,llocsDisponibles,duracioReservesEntreSetmana,horesPermesesReservarEntreSetmana, duracioReservesCapSetmana, horesPermesesReservarCapSetmana, paginaReserva) VALUES 
('Gimnàs 1', 41.389, 2.15, 'C/ Provença 8', '06:00 - 23:00', 14, 60, '06,08,10,12,18,19,20,21', 60, '10,14,18', 'reserva1.html'),
('Gimnàs 2', 41.388843, 2.148334, 'C/ Falsa 123', 'Obert 24h', 9, 60, '06,08,10,12,18,19,20,21', 30, '10,14,18', 'reserva2.html'),
('Gimnàs 3', 41.389655, 2.152262, 'C/ Marina 3', 'Obert 24h', 14, 125, '06,10,18,21', 35, '10,14,18', 'reserva3.html'),
('Gimnàs 4', 41.39, 2.155, 'C/ Villarroel 20', '10:00 - 22:00', 10, 60, '10,16', 0, '', 'reserva4.html'),
('Gimnàs 5', 41.391644, 2.150994, 'Carrer de París, 155', '10:00 - 22:00', 14, 0, '', 60, '18,20', 'reserva3.html'),
('CEX - Plaça Catalunya', 41.385748, 2.170265, 'Plaça de Catalunya, 1', '10:00 - 18:00', 10, 90, '10,12,14', 60, '10,14', 'reserva4.html'),
('Gimnàs Santa Coloma', 41.451017, 2.207830, 'Pompeu Fabra, 1', '06:00 - 23:00', 9, 120, '10,15,17,19', 45, '10,18', 'reserva2.html');
