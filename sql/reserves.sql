CREATE DATABASE "spinning_anywhere";

\connect "spinning_anywhere";

CREATE TABLE reserves(
gimnasID int,
usuariID character varying(20),
dia date,
hora int,
bici int,
FOREIGN KEY (gimnasID) REFERENCES gimnas(id),
FOREIGN KEY (usuariID) REFERENCES usuari(nom),
PRIMARY KEY (gimnasID, usuariID, dia, hora)
);

GRANT ALL PRIVILEGES ON reserves TO "chaptersix";
