CREATE DATABASE "spinning_anywhere";

\connect "spinning_anywhere";

CREATE TABLE usuari(
nom character varying(20) PRIMARY KEY,
pass character varying(40) NOT NULL,
mail character varying(40),
rol character varying(15),
gymsPreferits character varying(100)
);

GRANT ALL PRIVILEGES ON usuari TO "chaptersix";

INSERT INTO usuari (nom,pass,mail,rol,gymsPreferits) VALUES 
('admin','adminPass','admin@spinning.com','administrador','');
